# Низкоуровневое программирование. ЛР №3

## Задание

На базе данного транспортного формата описать схему протокола обмена информацией и воспользоваться существующей библиотекой по выбору для реализации модуля, обеспечивающего его функционирование. Протокол должен включать представление информации о командах создания, выборки, модификации и удаления данных в соответствии с данной формой, и результатах их выполнения.

Используя созданные в результате выполнения заданий модули, разработать в виде консольного приложения две программы: клиентскую и серверную части. Серверная часть – получающая по сети запросы и операции описанного формата и последовательно выполняющая их над файлом данных с помощью модуля из первого задания. Имя фала данных для работы получать с аргументами командной строки, создавать новый в случае его отсутствия. Клиентская часть – в цикле получающая на стандартный ввод текст команд, извлекающая из него информацию о запрашиваемой операции с помощью модуля из второго задания и пересылающая её на сервер с помощью модуля для обмена информацией, получающая ответ и выводящая его в человеко-понятном виде в стандартный вывод.

## Вариант
Apache Thrift

## Установка и запуск

### Необходимые реквизиты

- gcc
- flex
- bison
- glib-2.0
- gobjects-2.0
- thrift & thrift_c_glib

### Сборка и запуск

```shell
$ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/
$ thrift --gen c_glib query.thrift
$ cmake --build cmake-build-debug -j 6
```

```shell
$ ./cmake-build-debug/server_exe
$ ./cmake-build-debug/client_exe
```

## Структура проекта

### db_lib

Графовая база данных, реализованная в рамках лабораторной работы №1. Собирается в статическую библиотеку и предоставляет интерфейс для работы с БД. Подключается к серверу.

```c
Connection *connect();
QueryResult execute_db_query(Query query, Connection *connection);
void close_connection(Connection *connection);
```

### parser_lib

Парсер языка запросов Gremlin, реализованный в рамках лабораторной работы №2. Собирается в статическую библиотеку и предоставляет интерфейс для обработки входных данных. Подключается к клиенту.

```c
Query get_query(const char *string);
```

### gen-c_glib

Файлы, сгенерированные с помощью утилиты thrift. Собирается в статическую библиотеку и предоставляет интерфейс для клиент-серверного взаимодействия. Подключается к клиенту и серверу.

### Сервер

#### server

Отвечает за принятие и передачу данных клиенту.

#### server_convert

Сериализует структуры сервера в структуры в thrift и наоборот.

```c
Query convert_query(I_Query *query);
I_QueryResult *convert_result(QueryResult query_result, QueryType q_type);
```

### Клиент

#### client

Отвечает за принятие и передачу данных серверу.

#### client_convert

Сериализует структуры полученного дерева в результате парсинга запроса в структуры thrift и за печать полученных результатов.

```c
I_Query *convert_query(Query *query);
void print_result(I_QueryResult *result);
```

## Протокол взаимодействия

```thrift
enum I_ValueType {
    VT_INTEGER
    VT_STRING
    VT_FLOATING
    VT_BOOLEAN
}

union I_uValue {
    1: i32 integer
    2: string string_
    3: double floating
    4: bool boolean
}

struct I_Value {
    1: I_ValueType type
    2: I_uValue value
}

struct I_Property {
    1: optional string field
    2: I_Value value
}

struct I_Attribute {
    1: string name
    2: I_ValueType vt
}

struct I_Schema {
    1: string name
    2: list<I_Attribute> attributes
}

struct I_Node {
    1: optional string name
    2: list<I_Property> properties
}

enum I_LinkType {
    LT_TO
    LT_FROM
    LT_BOTH

    LT_ANY
}

struct I_Link {
    1: string name
    2: I_LinkType type
}

enum I_Comparation {
    CMP_GT
    CMP_LT
    CMP_GE
    CMP_LE
    CMP_EQ
    CMP_NEQ
    CMP_CONTAINS
}

enum I_ComparableType {
    CMPT_FIELD
    CMPT_VALUE
}

union I_uComparable {
    1: string field_name
    2: I_Value value
}

struct I_Comparable {
    1: I_ComparableType type
    2: I_uComparable comparable
}

struct I_Compare {
    1: I_Comparation cmp
    2: I_Comparable with_
}

struct I_ConditionCompare {
    1: I_Comparable left
    2: I_Compare compare
}

enum I_PredicateType {
    PT_TERM
    PT_OR
    PT_AND
}

union I_uPredicate {
    1: I_ConditionCompare term
    2: list<I_Predicate> and_
    3: list<I_Predicate> or_
}

struct I_Predicate {
    1: I_PredicateType type
    2: I_uPredicate predicate
}

struct I_NodeCondition {
    1: bool is_null
    2: string schema
    3: list<I_Predicate> predicates
}

struct I_LinkCondition {
    1: bool is_null

    2: I_LinkType link_type
    3: string link_name
}

struct I_MatchCondition {
    1: bool is_null

    2: I_NodeCondition node_first_condition
    3: I_NodeCondition node_second_condition
    4: I_NodeCondition node_cross_condition
    5: I_LinkCondition link_condition
}

enum I_QueryType {
    SCHEMA_CREATE
    SCHEMA_GET
    SCHEMA_GET_ALL
    SCHEMA_DELETE

    NODE_CREATE
    NODE_UPDATE
    NODE_DELETE
    NODE_MATCH

    LINK_CREATE
    LINK_DELETE

    MATCH
}

struct I_SchemaCreateQuery {
    1: I_Schema schema
}

struct I_SchemaGetQuery {
    1: string schema_name
}

struct I_SchemaGetAllQuery {}

struct I_SchemaDeleteQuery {
    1: string schema_name
}

struct I_NodeCreateQuery {
    1: string schema_name
    2: I_Node node
}

struct I_NodeUpdateQuery {
    1: I_NodeCondition condition
    2: list<I_Property> properties
}

struct I_NodeDeleteQuery {
    1: I_NodeCondition condition
}

struct I_NodeMatchQuery {
    1: I_NodeCondition condition
}

struct I_LinkCreateQuery {
    1: string link_name
    2: I_LinkType link_type
    3: I_NodeCondition first
    4: I_NodeCondition second
}

struct I_LinkDeleteQuery {
    1: I_MatchCondition condition
}

struct I_MatchQuery {
    1: I_MatchCondition condition
}

union I_uQuery {
    1: I_SchemaCreateQuery schema_create_query
    2: I_SchemaGetQuery schema_get_query
    3: I_SchemaGetAllQuery schema_get_all_query;
    4: I_SchemaDeleteQuery schema_delete_query

    5: I_NodeCreateQuery node_create_query
    6: I_NodeUpdateQuery node_update_query
    7: I_NodeDeleteQuery node_delete_query
    8: I_NodeMatchQuery node_match_query

    9: I_LinkCreateQuery link_create_query
    10: I_LinkDeleteQuery link_delete_query

    11: I_MatchQuery match_query
}

struct I_Query {
    1: I_QueryType type
    2: I_uQuery query
}

enum I_Return {
    RETURN_NOTHING      = 0,
    RETURN_NODE_FIRST   = 1,
    RETURN_NODE_SECOND  = 2,
    RETURN_LINK         = 4,
    RETURN_ALL          = 7,

    RETURN_UNDEF        = 8,
}

struct I_Result {
    1: I_Return return_items
    2: optional I_Node first
    3: optional I_Link link
    4: optional I_Node second
    5: optional I_Schema schema
}

enum I_QueryResultType {
    RESULT_SCHEMA
    RESULT_GENERATOR
    RESULT_NONE
}

union I_uQueryResult {
    1: I_Schema schema
    2: list<I_Result> items
}

struct I_QueryResult {
    1: I_QueryResultType type;
    2: string message;
    3: optional I_uQueryResult result;
}


service querySvc {
   I_QueryResult execute(1: I_Query query)
}
```

## Примеры запросов

### Схемы

#### Создание

```
> g.addS("students")
    .propertyKey("id").Int()
    .propertyKey("name").Text()
    .propertyKey("score").Float()
    .propertyKey("will_be_expelled").Bool();
Result code: 0 (RC_OK)
> g.addS("simple").propertyKey("id").Int();
Result code: 0 (RC_OK)
```

#### Получение всех

```
> g.S();
Result code: 0 (RC_OK)
0. Schema: students
- id: I__VALUE_TYPE_VT_INTEGER
- name: I__VALUE_TYPE_VT_STRING
- score: I__VALUE_TYPE_VT_FLOATING
- will_be_expelled: I__VALUE_TYPE_VT_BOOLEAN
1. Schema: simple
- id: I__VALUE_TYPE_VT_INTEGER
```

#### Получение схемы по имени

```
> g.S("students");
Result code: 0 (RC_OK)
Schema: students
- id: I__VALUE_TYPE_VT_INTEGER
- name: I__VALUE_TYPE_VT_STRING
- score: I__VALUE_TYPE_VT_FLOATING
- will_be_expelled: I__VALUE_TYPE_VT_BOOLEAN
```

#### Удаление схемы

```
> g.S("simple").drop();
Result code: 0 (RC_OK)
> g.S();
Result code: 0 (RC_OK)
0. Schema: students
- id: I__VALUE_TYPE_VT_INTEGER
- name: I__VALUE_TYPE_VT_STRING
- score: I__VALUE_TYPE_VT_FLOATING
- will_be_expelled: I__VALUE_TYPE_VT_BOOLEAN
```

### Вершины

#### Создание

```
> g.addV("students")
    .property("id", 1)
    .property("name", "Alex")
    .property("score", 4.8)
    .property("will_be_expelled", false);
Result code: 0 (RC_OK)
```

#### Получение

```
> g.V("students");
Result code: 0 (RC_OK)
0. [         1      Alex  4.800000     false]
1. [         2     Boris  4.900000     false]
2. [         3     Lyoha  3.100000      true]
3. [         4     Kolya  4.200000      true]
```

```
> g.V("students").has("score", gt(4));
Result code: 0 (RC_OK)
0. [         1      Alex  4.800000     false]
1. [         2     Boris  4.900000     false]
2. [         4     Kolya  4.200000      true]
> g.V("students").and(
    has("will_be_expelled", eq(true)),
    has("score", gt(4))
);
Result code: 0 (RC_OK)
0. [         4     Kolya  4.200000      true]
```

#### Обновление

```
> g.V("students")
    .has("score", gt(4))
    .property("score", 5);
> g.V("students");
Result code: 0 (RC_OK)
0. [         1      Alex  5.000000     false]
1. [         2     Boris  5.000000     false]
2. [         3     Lyoha  3.100000      true]
3. [         4     Kolya  5.000000      true]
```

#### Удаление

```
> g.V("students").has("will_be_expelled", eq(true));
Result code: 0 (RC_OK)
0. [         3     Lyoha  3.100000      true]
1. [         4     Kolya  5.000000      true]

> g.V("students").has("will_be_expelled", eq(true)).drop();
Result code: 0 (RC_OK)
> g.V("students");
Result code: 0 (RC_OK)
0. [         1      Alex  5.000000     false]
1. [         2     Boris  5.000000     false]
```

### Ребра

#### Создание

```
> g.addE("FRIENDS").both(
    g.V("students").has("id", eq(1)), 
    g.V("students").has("id", eq(2))
);
Result code: 0 (RC_OK)
```

#### Получение

```
> g.V("students").both("FRIENDS");
Result code: 0 (RC_OK)
0. [         1      Alex  5.000000     false]<--[FRIENDS]-->[         2     Boris  5.000000     false]
1. [         1      Alex  5.000000     false]<--[FRIENDS]-->[         3     Pasha  4.700000     false]
```

```
> g.V("students").has("score", eq(5))
    .both("FRIENDS")
    .V("students").has("score", eq(5));
Result code: 0 (RC_OK)
0. [         1      Alex  5.000000     false]<--[FRIENDS]-->[         2     Boris  5.000000     false]
```

#### Удаление

```
> g.V("students").has("score", eq(5))
    .both("FRIENDS")
    .V("students").has("score", lt(4.9))
    .dropE();
Result code: 0 (RC_OK)
```

```
> g.V("students").both("FRIENDS");
Result code: 0 (RC_OK)
0. [         1      Alex  5.000000     false]<--[FRIENDS]-->[         2     Boris  5.000000     false]
```

## Выводы

Я познакомился с клиент-серверным взаимодействием на языке Си с использованием RPC-фреймворка Apache Thrift. Наверное, это была самая неприятная лаба из всех трёх, потому что было много обезьяней работы в виде сериализации: Клиент<->Thrift, Thrift<->Сервер.

Также обнаружил несколько недочётов в предыдущих работах, поэтому пришлось еще и их доделывать. В любом случае это было интересно и полезно, поэтому все круто!)
